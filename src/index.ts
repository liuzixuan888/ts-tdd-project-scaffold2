//获取“普通商品”浮动价值 
const getNormalFloatingPrice = (currentSellIn: number) => {
  return currentSellIn > 1 ? -1 : -2
}

//获取“后台门票”浮动价值 
const getBackstagePassFloatingPrice = (currentSellIn: number, currentQuality: number) => {
  let price = 0
  if (currentSellIn <= 0) {
    // 过了演出日
    price = -currentQuality
  } else if (currentSellIn <= 5) {
    // 演出当日及距演出5天内
    price = 3
  } else if (currentSellIn <= 10) {
    // 距演出10天内
    price = 2
  } else if (currentSellIn <= 15) {
    // 距演出15天内
    price = 1
  } else {
    // 距演出大于15天，不贬值也不升值
    price = 0
  }
  return price
}

//获取“陈年干酪”浮动价值
const getAgedBrieFloatingPrice = (currentSellIn: number) => {
  return currentSellIn <= 0 ? 2 : 1
}


// 目标输出第二天的价值
const queryInventoryValue = (goodsType: string, currentSellIn: number, currentQuality: number) => {
  if (currentQuality > 50 || currentQuality < 0) {
    throw new RangeError("商品的价值永远不会小于 0，也永远不会超过 50!");
  }
  let floatingPrice = 0 //价格浮动额度
  switch (goodsType) {
    case 'normalGoods':
      floatingPrice = getNormalFloatingPrice(currentSellIn)
      break;
    case 'backstagePass':
      floatingPrice = getBackstagePassFloatingPrice(currentSellIn, currentQuality)
      break
    case 'sulfuras':
      floatingPrice = 0
      break
    case 'agedBrie':
      floatingPrice = getAgedBrieFloatingPrice(currentSellIn)
      break
    default:
      throw new RangeError("您输入的的商品类型有误")
  }
  currentQuality = currentQuality + floatingPrice
  // 限制价值额度，最小0，最大50
  currentQuality = currentQuality > 50 ? 50 : currentQuality < 0 ? 0 : currentQuality
  return currentQuality
}
export { queryInventoryValue }