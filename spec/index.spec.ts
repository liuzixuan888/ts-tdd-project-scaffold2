import { queryInventoryValue } from '../src'
describe("The second-day value of normal goods", () => {
    test("Given that the expiration date of conventional goods is 10 days, the current value is 20, and the expected output value of the next day is 19.", () => {
        expect(queryInventoryValue('normalGoods', 10, 20)).toBe(19);
    });
    test("Given that the expiration date of conventional goods is 3 days, the current value is 6, and the expected output value of the next day is 5.", () => {
        expect(queryInventoryValue('normalGoods', 3, 6)).toBe(5);
    });
    test("Given that the expiration date of conventional goods is -1 days, the current value is 6, and the expected output value of the next day is 4.", () => {
        expect(queryInventoryValue('normalGoods', -1, 6)).toBe(4);
    });
    test("Given that the expiration date of conventional goods is 0 days, the current value is 6, and the expected output value of the next day is -1.", () => {
        expect(queryInventoryValue('normalGoods', 0, 6)).toBe(4);
    });
});


describe("The second-day value of backstagePass", () => {
    test("Given that the expiration date of backstagePass is 15 days, the current value is 20, and the expected output value of the next day is 21.", () => {
        expect(queryInventoryValue('backstagePass', 15, 20)).toBe(21);
    });
    test("Given that the expiration date of backstagePass is 10 days, the current value is 45, and the expected output value of the next day is 47.", () => {
        expect(queryInventoryValue('backstagePass', 10, 45)).toBe(47);
    });
    test("Given that the expiration date of backstagePass is 9 days, the current value is 45, and the expected output value of the next day is 47.", () => {
        expect(queryInventoryValue('backstagePass', 9, 45)).toBe(47);
    });
    test("Given that the expiration date of backstagePass is 10 days, the current value is 49, and the expected output value of the next day is 50.", () => {
        expect(queryInventoryValue('backstagePass', 10, 49)).toBe(50);
    });
    test("Given that the expiration date of backstagePass is 10 days, the current value is 50, and the expected output value of the next day is 50.", () => {
        expect(queryInventoryValue('backstagePass', 10, 50)).toBe(50);
    });
    test("Given that the expiration date of backstagePass is 5 days, the current value is 49, and the expected output value of the next day is 50.", () => {
        expect(queryInventoryValue('backstagePass', 5, 49)).toBe(50);
    });
    test("Given that the expiration date of backstagePass is 5 days, the current value is 45, and the expected output value of the next day is 48.", () => {
        expect(queryInventoryValue('backstagePass', 5, 45)).toBe(48);
    });
    test("Given that the expiration date of backstagePass is 1 days, the current value is 20, and the expected output value of the next day is 23.", () => {
        expect(queryInventoryValue('backstagePass', 1, 20)).toBe(23);
    });
    test("Given that the expiration date of backstagePass is 0 days, the current value is 20, and the expected output value of the next day is 0.", () => {
        expect(queryInventoryValue('backstagePass', 0, 20)).toBe(0);
    });
});

describe("The second-day value of sulfuras", () => {
    test("Given that the expiration date of sulfuras is 0 days, the current value is 45, and the expected output value of the next day is 45.", () => {
        expect(queryInventoryValue('sulfuras', 0, 45)).toBe(45);
    });
    test("Given that the expiration date of sulfuras is -1 days, the current value is 45, and the expected output value of the next day is 45.", () => {
        expect(queryInventoryValue('sulfuras', -1, 45)).toBe(45);
    });
    test("Given that the expiration date of sulfuras is 9 days, the current value is 45, and the expected output value of the next day is 47.", () => {
        expect(queryInventoryValue('sulfuras', -1, 50)).toBe(50);
    });
    test("Given that the expiration date of sulfuras is -1 days, the current value is 1, and the expected output value of the next day is 1.", () => {
        expect(queryInventoryValue('sulfuras', -1, 1)).toBe(1);
    });
    test("Given that the expiration date of sulfuras is -2 days, the current value is 1, and the expected output value of the next day is 1.", () => {
        expect(queryInventoryValue('sulfuras', -2, 1)).toBe(1);
    });
});

describe("The second-day value of agedBrie", () => {
    test("Given that the expiration date of agedBrie is 2 days, the current value is 0, and the expected output value of the next day is 1.", () => {
        expect(queryInventoryValue('agedBrie', 2, 0)).toBe(1);
    });
    test("Given that the expiration date of agedBrie is 2 days, the current value is 49, and the expected output value of the next day is 50.", () => {
        expect(queryInventoryValue('agedBrie', 2, 49)).toBe(50);
    });
    test("Given that the expiration date of agedBrie is 0 days, the current value is 20, and the expected output value of the next day is 22.", () => {
        expect(queryInventoryValue('agedBrie', 0, 20)).toBe(22);
    });
    test("Given that the expiration date of agedBrie is -1 days, the current value is 20, and the expected output value of the next day is 22.", () => {
        expect(queryInventoryValue('agedBrie', -1, 20)).toBe(22);
    });
});
